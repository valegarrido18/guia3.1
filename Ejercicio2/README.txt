>> Ejercicio 2

Al ejecutar tiene un menu con dos opciones, la opcion 1 permite ingresar una palabra a la lista y la opcion 2 permite salir.
El programa solo recibe tipo string a la lista. 


    • REQUISITOS PREVIOS
     
Sistema operativo Linux 
GNU Make (para compilar) 


-- EJECUTANDO LAS PRUEBAS POR TERMINAL --
Para compilar el código se debe agregar el comando make por terminal, luego de realizar la compilación, se utiliza el comando ./programa para ejecutar.

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.


    • VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

    • AUTORES

Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.
