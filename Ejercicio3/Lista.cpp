#include <iostream>
using namespace std;

#include "Ejemplo.h"
#include "Lista.h"


Lista::Lista() {}


void Lista::crear (Postre *postre) {
    Nodo *tmp;

    /* crea un nodo . */
    tmp = new Nodo;
    /* asigna la instancia de postre. */
    tmp->postre = postre;
    /* apunta a NULL por defecto. */
    tmp->sig = NULL;

    /* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    /* de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista. */
    } else {
        this->ultimo->sig = tmp;
        this->ultimo = tmp;
    }
}


void Lista::imprimir () {  
    /* utiliza variable temporal para recorrer la lista. */
    Nodo *tmp = this->raiz;
    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    while (tmp != NULL) {
        cout << "[" <<tmp->postre->get_postre() << "]" << endl;
        tmp = tmp->sig;
    }
}
