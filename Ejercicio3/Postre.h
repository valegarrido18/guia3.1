#include <iostream>
using namespace std;

#ifndef POSTRE_H
#define POSTRE_H

class Postre {
    private:
        string postre = "\0";

    public:
        /* constructor */
        Postre(string postre);
        
        /* método get */
        string get_postre();
};
#endif
