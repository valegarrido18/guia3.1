#include <iostream>
using namespace std;

#ifndef NUMERO_H
#define NUMERO_H

class Numero {
    private:
        int numero = 0;

    public:
        /* constructor */
        Numero(int numero);
        
        /* método get */
        int get_numero();
};
#endif
